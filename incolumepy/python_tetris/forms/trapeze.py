from inspect import stack


def trapeze(*args, **kwargs) -> tuple:
    """Return form."""
    ...
    return stack()[0][3], args, kwargs,
