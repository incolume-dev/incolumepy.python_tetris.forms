from incolumepy.python_tetris.forms import __version__


def test_version():
    assert __version__ == '0.1.0'
